<?php
    function pretty_print($in, $opened = true)
    {
        if ($opened)
            $opened = ' open';
        if (is_object($in) or is_array($in)) {
            echo '<div>';
            echo '<details' . $opened . '>';
            echo '<summary>';
            echo (is_object($in)) ? 'Object {' . count((array)$in) . '}' : 'Array [' . count($in) . ']';
            echo '</summary>';
            pretty_print_rec($in, $opened);
            echo '</details>';
            echo '</div>';
        } elseif (is_string($in)) {
            echo $in . " (" . gettype($in) . ')' . "<br>";
        } else echo "no print!";
    }

    function pretty_print_rec($in, $opened, $margin = 10)
    {
        if (!is_object($in) && !is_array($in))
            return;

        foreach ($in as $key => $value) {
            if (is_object($value) or is_array($value)) {
                echo '<details style="margin-left:' . $margin . 'px" ' . $opened . '>';
                echo '<summary>';
                echo (is_object($value)) ? $key . ' {' . count((array)$value) . '}' : $key . ' [' . count($value) . ']';
                echo '</summary>';
                pretty_print_rec($value, $opened, $margin + 10);
                echo '</details>';
            } else {
                switch (gettype($value)) {
                    case 'string':
                        $bgc = 'red';
                        break;
                    case 'integer':
                        $bgc = 'green';
                        break;
                }
                echo '<div style="margin-left:' . $margin . 'px">' . $key . ' : <span style="color:' . $bgc . '">' . $value . '</span> (' . gettype($value) . ')</div>';
            }
        }
    }

function buildContentTable($table_array, $width = false):string
{
    if (is_array($table_array) && is_array($table_array['header']) && is_array($table_array['body']) && (count($table_array['header']) == count($table_array['body'][0]))) {
        $width = ($width) ? "style = 'width: " . $width . "'" : "";
        $ret = '<tr>';
        foreach ($table_array['header'] as $th) {
            $ret .= buildTh($th);
        }
        $ret .= '</tr>';
        foreach ($table_array['body'] as $k => $row) {
            $tr_class = (($k % 2) == 0) ? "class='COLOR-SELECT'" : "";
            $ret .= "<tr $tr_class>";
            foreach ($row as $td) {
                $ret .= buildTd($td);
            }
            $ret .= '</tr>';
        }
        $ret = "<table class='content' " . $width . ">" . $ret . "</table>";
    } else {
        $ret = false;
    }
    return $ret;
}

function buildTh($th)
{
    return "<th>" . $th . "</th>";
}

function buildTd($td)
{
    return "<td>" . $td . "</td>";
}
