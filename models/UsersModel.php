<?php

namespace models;
require_once('../database/Db.php');

class UsersModel extends Db
{
    private $all_users;

    function __construct()
    {
        parent::__construct();
    }

    private function createAllUsers()
    {
        $query = "select * from users order by id desc ";
        $this->all_users = $this->execQuery($query)->fetchAll();
    }

    public function getAllUsers()
    {
        if (empty($this->all_users)) {
            self::createAllUsers();
        }
        return $this->all_users;
    }
}