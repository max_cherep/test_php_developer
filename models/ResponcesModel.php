<?php


namespace models;
require_once('../database/Db.php');

class ResponcesModel extends Db
{
    private $all_responses;

    function __construct()
    {
        parent::__construct();
    }

    private function createAllResponses()
    {
        $sort = ($_SESSION['sort']) ? "desc" : "asc";
        $query = "select * from responses order by created_at $sort";
        $this->all_responses = $this->execQuery($query)->fetchAll();
    }

    public function getAllResponses()
    {
        if (empty($this->all_responses)) {
            self::createAllResponses();
        }
        return $this->all_responses;
    }

    public function saveResponseToDb($response)
    {
        $user_name = $response['user'];
        $response_text = $response['text'];
        $image_path = $response['image_path'];
        $thumbnail_path = $response['thumbnail_path'];
        $query = "insert into responses (user_name, response_text, image_path, thumbnail_path) values ('$user_name','$response_text', '$image_path', '$thumbnail_path')";
        $this->execQuery($query);
    }

    public function makeApproveToDB($id)
    {
        $query = "update responses set is_approved = '1' where id=$id";
        $this->execQuery($query);
    }

    public function makeNotApproveToDB($id)
    {
        $query = "update responses set is_approved = '0' where id=$id";
        $this->execQuery($query);
    }

    public function getResponseFromId($id): string
    {
        $query = "select response_text from responses where id=$id";
        return $this->execQuery($query)->fetch()['response_text'];
    }

    public function editResponseFromDB($id, $text)
    {
        $query = "update responses set response_text = '$text' where id=$id";
        $this->execQuery($query);
        $query = "update responses set is_edited = '1' where id=$id";
        $this->execQuery($query);
        $query = "update responses set updated_at = now() where id=$id";
        $this->execQuery($query);
    }
}