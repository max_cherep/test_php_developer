<?php
header('Pragma: no-cache');
require_once('api/GetRequest.php');
$ear = new GetRequest();
if ($ear->is_json) {
    $metric = json_decode($ear->request);
    require_once('../models/UsersModel.php');
    require_once('../controllers/UserController.php');
    $user_controller = new controller\UserController();
    require_once('../models/ResponcesModel.php');
    require_once('../controllers/ResponsesController.php');
    $response_controller = new \controller\ResponsesController();
    $check = $user_controller->checkToken($metric);
    if ($check) {
        $response = [
            'user' => $metric->user,
            'text' => $metric->response_text,
            'image_path' => '',
            'thumbnail_path' => '',
        ];
        $response_controller->saveResponse($response);
        GetRequest::sendHeader('200');
        die();
    } else {
        GetRequest::sendHeader('403');
        die();
    }
}
GetRequest::sendHeader('403');
die();