<?php
session_start();
if ($_GET['action'] == "logout") {
    unset($_SESSION['user_name']);
    unset($_SESSION['is_auth']);
    unset($_SESSION['is_admin']);
} else {
    if (isset($_POST['login']) && $_POST['login'] != '' && isset($_POST['password']) && $_POST['password'] != '') {
        $is_auth = false;
        $is_admin = false;
        require_once('../models/UsersModel.php');
        $users = new \models\UsersModel();
        $all_users = $users->getAllUsers();
        foreach ($all_users as $user) {
            if ($user['name'] == $_POST['login'] && $user['password'] == $_POST['password']) {
                $is_auth = true;
                $name = $user['name'];
                if ($user['role'] == "a") $is_admin = true;
                break;
            }
        }
        if ($is_auth) {
            $_SESSION['is_auth'] = true;
            $_SESSION['user_name'] = $name;
            if ($is_admin) $_SESSION['is_admin'] = true;
            else
                $_SESSION['is_admin'] = false;
        }
    }
}
header('Location: http://' . $_SERVER['HTTP_HOST'] . '/');