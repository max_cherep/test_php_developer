<?php

class GetRequest

{
    function __construct()
    {
        $this->request = file_get_contents("php://input");
        $this->is_json = self::isJson($this->request);
    }
    private static function isJson($string)
    {
        json_decode($string);
        return (json_last_error() == JSON_ERROR_NONE);
    }

    static function sendHeader($code)
    {
        switch ($code) {
            case '200':
                header('HTTP/1.1 200 OK');
                break;
            case '307':
                header('HTTP/1.1 307 Temporary Redirect');
                break;
            case '401':
                header('HTTP/1.1 401 Unauthorized');
                break;
            case '403':
                header('HTTP/1.0 403 Forbidden');
                break;
            case '404':
                header('HTTP/1.0 404 Not Found');
                break;
            case '503':
                header('HTTP/1.0 503 Service Unavailable');
                break;
            default:
                header('HTTP/1.0 500 Internal Server Error');

        }
    }
}


