<?php
/**
 * test php
 * (C) M.Cherepanov
 */
?>
<link rel='stylesheet' href='css/style.css'>
<link rel='stylesheet' href='css/my_style.css'>
<?php
require_once('../views/ResponsesView.php');
require_once('../develop/services.php');
use view\ResponsesView as viewResponses;
$view = new viewResponses();
session_start();
?>
<header>
    <?php print($view->getHeader()); ?>
</header>
<nav>
    <?php print($view->getNav()); ?>
    <?php print($view->getLogin()); ?>
</nav>
<section>
    <?php if($_SESSION['is_admin'] == false) print($view->getResponseForm()); ?>
    <?php print($view->getAllResponsesTable($_SESSION['sort'])); ?>
</section>
<footer>
    <?php print($view->getFooter()); ?>
</footer>

