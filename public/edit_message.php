<?php
session_start();
if (!$_SESSION['is_admin'])
    header('Location: http://' . $_SERVER['HTTP_HOST'] . '/');
elseif ($_GET['action'] == "edit" && $_GET['id'] != '' && isset($_GET['response_text'])) {
    require_once('../controllers/ResponsesController.php');
    $controller = new \controller\ResponsesController();
    $controller->editResponse($_GET['id'], $_GET['response_text']);
    header('Location: http://' . $_SERVER['HTTP_HOST'] . '/');
}
