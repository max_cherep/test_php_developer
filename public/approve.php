<?php
session_start();
if (!$_SESSION['is_admin'])
    header('Location: http://' . $_SERVER['HTTP_HOST'] . '/');
elseif ($_GET['action'] != "" && $_GET['id'] != '') {
    require_once('../controllers/ResponsesController.php');
    $controller = new \controller\ResponsesController();
    switch ($_GET['action']){
        case 'approve':
        case 'not_approve':
            $controller->makeApprove($_GET['action'], $_GET['id']);
            break;
        default:
    }
}
header('Location: http://' . $_SERVER['HTTP_HOST'] . '/');

