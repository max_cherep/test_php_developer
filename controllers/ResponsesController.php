<?php


namespace controller;
require_once('../models/ResponcesModel.php');
require_once('../tools/SimpleImage.php');

use models\ResponcesModel as ResponsesModel;

class ResponsesController extends ResponsesModel
{
    function __construct()
    {
        parent::__construct();
    }

    public function checkResponse(): array
    {
        $check_response = $_POST;
        $ret = [];
        if (isset($check_response['name']) && $check_response['name'] != '' && isset($check_response['response']) && $check_response['response'] != '') {
            $ret['user'] = $check_response['name'];
            $ret['text'] = $check_response['response'];
            $ret = array_merge($ret, self::createImageAndThumbnail());
        }
        return $ret;
    }

    public function saveResponse($response)
    {
        self::saveResponseToDb($response);
    }

    private function createImageAndThumbnail()
    {
        $ret = [];
        if (isset ($_FILES['image']) && $_FILES['image']['name'] != '') {
            $image = new \SimpleImage();
            if (end(explode('.', $_FILES['image']['name'])) == 'png' || end(explode('.', $_FILES['image']['name'])) == 'jpg') {
                $ret['image_path'] = '/images/' . md5($ret['user']) . "_" . time() . "." . end(explode('.', $_FILES['image']['name']));
                $ret['thumbnail_path'] = '/images/thumbnail/' . md5($ret['user']) . "_" . time() . "." . end(explode('.', $_FILES['image']['name']));
                move_uploaded_file($_FILES['image']["tmp_name"], '/var/www/test/files' . $ret['image_path']);
                #
                $image->load('/var/www/test/files' . $ret['image_path']);
                if ($image->getHeight() >= $image->getWidth()) { // portrait or square
                    if ($image->getHeight() > 640) {
                        $image->resizeToHeight(640);
                        $image->save('/var/www/test/files' . $ret['image_path']);
                    }
                } else { //landscape
                    if ($image->getWidth() > 480) {
                        $image->resizeToWidth(480);
                        $image->save('/var/www/test/files' . $ret['image_path']);
                    }
                }
                if ($image->getHeight() > 640 || $image->getWidth() > 480) $image->scale(10);
                else $image->resize(40, 40);
                $image->save('/var/www/test/files' . $ret['thumbnail_path']);
            }
        } else {
            $ret['image_path'] = NULL;
            $ret['thumbnail_path'] = NULL;
        }
        return $ret;
    }

    public function makeApprove($action, $id)
    {
        switch ($action) {
            case 'approve':
                self::makeApproveToDB($id);
                break;
            case 'not_approve':
                self::makeNotApproveToDB($id);
                break;
        }
    }

    public function editResponse($id, $text)
    {
        self::editresponseFromDB($id, $text);
    }
}
