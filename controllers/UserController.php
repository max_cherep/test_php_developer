<?php


namespace controller;

use models\UsersModel as UserModel;

class UserController extends UserModel
{
    function __construct()
    {
        parent::__construct();
    }

    public function checkToken($metric)
    {
        $users = parent::getAllUsers();
        $valid = false;
        foreach ($users as $user) {
            if ($metric->token == $user['token'] && $metric->user == $user['name'] && $metric->email == $user['email'] ) {
                $valid = true;
                break;
            }
        }
        return $valid;
    }
}