<?php
namespace models;

class Db
{
    function __construct()
    {
        $this->host = 'localhost';
        $this->database = 'test';
        $this->user = 'test';
        $this->password = 'test-pass';
        $this->opt = [
            \PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION,
            \PDO::ATTR_DEFAULT_FETCH_MODE => \PDO::FETCH_ASSOC,
            \PDO::ATTR_EMULATE_PREPARES => false,
        ];
        $this->error_answer = array(['error' => true]);
        $this->ok_answer = array(['status' => 'ok']);
        $this->connect = $this->getConnect();
    }

    private function getConnect()
    {
        try {
            $db = new \PDO('mysql:host=' . $this->host . ';dbname=' . $this->database, $this->user, $this->password, $this->opt);
        } catch (\PDOException $e) {
            echo "error connect";
        }
        return $db;
    }

    public function execQuery($query)
    {
        $stmt = $this->connect->prepare($query);
        $stmt->execute();
        return $stmt;
    }
}