SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

SET NAMES utf8mb4;

DROP TABLE IF EXISTS `responses`;
CREATE TABLE `responses` (
                             `id` bigint(20) NOT NULL AUTO_INCREMENT,
                             `user_id` bigint(20) unsigned DEFAULT NULL,
                             `user_name` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                             `response_text` varchar(2000) COLLATE utf8mb4_unicode_ci NOT NULL,
                             `image_path` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
                             `thumbnail_path` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
                             `is_approved` char(1) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
                             `is_edited` char(1) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
                             `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
                             `updated_at` timestamp NOT NULL DEFAULT current_timestamp(),
                             `approved_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE current_timestamp(),
                             PRIMARY KEY (`id`),
                             KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
                         `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
                         `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
                         `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
                         `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
                         `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
                         `hash` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
                         `role` varchar(1) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'u',
                         `created_at` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp(),
                         `updated_at` timestamp NULL DEFAULT NULL,
                         PRIMARY KEY (`id`),
                         UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
