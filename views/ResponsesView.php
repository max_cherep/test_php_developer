<?php


namespace view;
require_once('../models/ResponcesModel.php');
require_once('../develop/services.php');

use models\ResponcesModel;

class ResponsesView extends ResponcesModel
{
    function __construct()
    {
        parent::__construct();
    }

    private function createAllResponsesTable($sort)
    {
        $sort = "<a href='change_sort.php'>Дата и время</a>";
        $table_array['header'] = (self::isAdmin()) ? ['Имя', $sort, 'Текст', 'Состояние', 'Изображение', 'Статус'] : ['Имя', $sort, 'Текст', 'Состояние', 'Изображение'];
        $table_array['body'] = [];
        $responses = self::getAllResponses();
        foreach ($responses as $response) {
            $is_edited = ($response['is_edited'] == '1') ? "Изменено админом " . $response['updated_at'] : "Оригинал";
            $form = self::createApprovedForm($response['id'], $response['response_text']);
            $link = ($response['image_path'] != "") ? '<a href="' . $response['image_path'] . '"><img src="' . $response['thumbnail_path'] . '"></a>' : "Нет картинки";
            $approve = ($response['is_approved'] == "1")
                ? "<a href='approve.php?action=not_approve&id=" . $response['id'] . "'>Отменить</a>"
                : "<a href='approve.php?action=approve&id=" . $response['id'] . "'>Утвердить</a>";
            $new_element = (self::isAdmin())
                ? [$response['user_name'], $response['created_at'], $form, $is_edited, $link, $approve]
                : (($response['is_approved'] != 0) ? [$response['user_name'], $response['created_at'], $response['response_text'], $is_edited, $link] : false);
            if ($new_element != false) $table_array['body'][] = $new_element;
        }
        return buildContentTable($table_array);
    }

    private function createApprovedForm($id, $text)
    {
        $form = "<form action='edit_message.php' method='get'>
        <textarea rows='3' cols='20' name='response_text'>$text</textarea>
        <input type='hidden' name='action' value='edit'>
        <input type='hidden' name='id' value='$id'>
        <br><input type='submit' value='Сохранить изменения'>
</form>";
        return $form;
    }

    public function getAllResponsesTable($sort = false)
    {
        return self::createAllResponsesTable($sort);
    }

    private function createResponseForm()
    {
        $response_form = "<form action='answer.php' enctype='multipart/form-data' method='post'>";
        $response_form .= "<p>Введите сообщение,<br>оно появится только после модерации</p>";
        $response_form .= "<p><label for='name'>Имя (до 200 символов)</label><br><input type='text' id='name' name='name'></p>";
        $response_form .= "<p><label for='response'>Введите текст сообщения (до 2000 символов)</label><br>
                            <textarea id='response' name='response'></textarea></p>";
        $response_form .= "<p><label for='image'>Выберите картинку, только png или jpg</label><br>
                            <input type='file' value='Картинка' accept='.png, .jpg' id='image' name='image'></p>";
        $response_form .= "<p><input type='submit' id='submit' name='submit' value='Отправить на модерацию'></p>";
        $response_form .= "</form>";
        return $response_form;
    }

    public function getResponseForm()
    {
        return self::createResponseForm();
    }

    private function createNav()
    {

        $nav = '
    <h2>Навигация</h2><ul>
        <li><a href="#">ссылка 1</a></li>
        <li><a href="#">ссылка 2</a></li>
        <li><a href="#">ссылка 3</a></li>
        <li><a href="#">ссылка 4</a></li>
        </ul>';
        return $nav;
    }

    public function getNav()
    {
        return self::createNav();
    }

    private function createLogin()
    {
        if (self::isAuth() && $_SESSION['user_name'] != "") {
            $greetings = "<p>Привет, " . $_SESSION['user_name'] .
                "<form action='login.php' method='get'>
                <input type='hidden' name='action' value='logout'>
                <input type='submit' value='Выйти' name='log_in'>
                </form>";
        } else {
            $greetings = "<form action='login.php' method='post'><table>
<tr><td>Логин: </td><td><input type='text' name='login'></td></tr>
<tr><td>Пароль: </td><td><input type='password' name='password'></td></tr>
<tr><td colspan='2'><input type='submit' value='Войти' name='log_in'></td></tr>
</table>



 </form>";
        }
        return $greetings;
    }

    public function getLogin()
    {
        return self::createLogin();
    }

    private function createHeader()
    {
        $header = "<h1>Заголовок</h1>";
        return $header;
    }

    public function getHeader()
    {
        return self::createHeader();
    }

    private function createFooter()
    {
        $footer = "<h2>Подвал</h2>";
        return $footer;
    }

    public function getFooter()
    {
        return self::createFooter();
    }

    private function isAdmin(): bool
    {
        if ($_SESSION['is_admin']) return true;
        else return false;
    }

    private function isAuth(): bool
    {
        if ($_SESSION['is_auth']) return true;
        else return false;
    }

    public function makeEditResponse($id): string
    {
        $ret = self::getResponseFromId($id);
        $ret = "<form action='edit_message' method='get'>
        <input type='text' name='text' value='" . $ret . "'>
        <input type='hidden' name='action' value='save'>
        <input type='hidden' name='id' value='" . $id . "'>
        <input type='submit'>
        </form>";
        return $ret;
    }
}